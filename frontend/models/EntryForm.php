<?php

namespace app\models;

use Yii;
use yii\base\Model;

class EntryForm extends Model
{
    public $fname;
    public $lname;
    public $phone;
    public $email;
    public $color;
    public $dob;
    public $gender;
    public $qual;
    public $pass;
    public $cpass;
    public $resume;

    public function rules()
    {
        return [
            [['fname', 'lname', 'phone', 'email', 'dob', 'gender', 'qual', 'pass', 'cpass', 'resume'], 'required'],
            ['email', 'email'],
            ['phone', 'number'],
            [['fname', 'lname', 'phone', 'email'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            ['dob', 'date', 'format' => 'php:Y-m-d'],
            ['cpass', 'compare', 'compareAttribute' => 'pass'],
            ['resume', 'file', 'extensions' => ['png', 'jpg', 'gif'], 'maxSize' => 1024 * 1024],
            ['dob', 'match', 'pattern' => '/^\d{4}\-(0[1-9]|1[012])\-(0[1-9]|[12][0-9]|3[01])$/'],
            ['phone', 'match', 'pattern' => '/^[7-9]\d{9}$/']

        ];
    }
}
