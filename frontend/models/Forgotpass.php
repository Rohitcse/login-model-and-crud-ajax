<?php

namespace frontend\models;

//use yii\base\InvalidArgumentException;
use yii\base\Model;
use Yii;
use common\models\User;

class Forgotpass extends Model
{
    public $email;
    public $new_password;
    public $confirm_password;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            [
                'email', 'exist',
                'targetClass' => '\common\models\User',
                'filter' => ['status' => User::STATUS_ACTIVE],
                'message' => 'There is no user with this email address.'
            ],

            [['new_password', 'confirm_password', 'email'], 'required'],
            [['new_password', 'confirm_password'], 'filter', 'filter' => 'trim'],
            [['new_password', 'confirm_password'], 'string', 'min' => 3],
            [['confirm_password'], 'compare', 'compareAttribute' => 'new_password', 'message' => 'Password does not match'],
        ];
    }

    public function verifyCurrentPassword()
    {
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }
        $pp = $user->password_hash;
        if ($user->validatePassword($this->old_password, $pp))
            return true;
    }

    public function resetPassword()
    {
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);
        if (!$user) {
            return false;
        }
        $user->setPassword($this->new_password);
        //$user->save(false);
        return $user->save(false);
    }

    /**
     * Sends an email with a link, for resetting the password.
     *
     * @return bool whether the email was send
     */
    public function sendEmail()
    {
        /* @var $user User */
        $user = User::findOne([
            'status' => User::STATUS_ACTIVE,
            'email' => $this->email,
        ]);

        if (!$user) {
            return false;
        }

        if (!User::isPasswordResetTokenValid($user->password_reset_token)) {
            $user->generatePasswordResetToken();
            if (!$user->save()) {
                return false;
            }
        }

        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'passwordResetToken-html', 'text' => 'passwordResetToken-text'],
                ['user' => $user]
            )
            ->setFrom([Yii::$app->params['supportEmail'] => Yii::$app->name . ' robot'])
            ->setTo($this->email)
            ->setSubject('Password reset for ' . Yii::$app->name)
            ->send();
    }
}
