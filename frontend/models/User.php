<?php

namespace frontend\models;

use yii\db\ActiveRecord;

class User extends ActiveRecord
{
    public function rules()
    {
        return [
            [['photo'], 'string', 'max' => 200],
        ];
    }
}
