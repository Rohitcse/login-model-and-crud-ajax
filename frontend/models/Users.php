<?php

namespace frontend\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $id
 * @property string $first_name
 * @property string $last_name
 * @property string $email
 * @property string $pwd
 * @property string $cpwd
 * @property string|null $photo
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name', 'email', 'pwd', 'cpwd'], 'required'],
            [['first_name', 'last_name'], 'string', 'max' => 30],
            ['email', 'email'],
            ['cpwd', 'compare', 'compareAttribute' => 'pwd'],
            [['first_name', 'last_name', 'email'], 'filter', 'filter' => 'trim', 'skipOnArray' => true],
            [['email'], 'string', 'max' => 50],
            [['pwd', 'cpwd'], 'string', 'max' => 20],
            [['photo'], 'string', 'max' => 200],
            [['email'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
            'email' => 'Email',
            'pwd' => 'Pwd',
            'cpwd' => 'Cpwd',
            'photo' => 'Photo',
        ];
    }
}
