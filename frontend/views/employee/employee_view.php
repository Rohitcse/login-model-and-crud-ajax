<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<!-- ['id' => $model->formName()] -->
<!-- [
    'options' => [
        'id' => 'create-product-form'
    ]
] -->
<h1>Add User</h1>
<?php $form = ActiveForm::begin(['id' => $model->formName()]); ?>

<?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'pwd')->passwordInput(['maxlength' => true])->label('Password') ?>

<?= $form->field($model, 'cpwd')->passwordInput(['maxlength' => true])->label('Confirm Password') ?>

<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>

<?php $script = <<< JS
$('form#{$model->formName()}').on('beforeSubmit',function(e)
{
    var \$form=$(this);
    $.post(
        \$form.attr("action"),
        \$form.serialize()
    )
    .done(function (result) {
        if(result==1)
        {
            //$(\$form).trigger("reset");
            $(document).find('#modal').modal('hide');
            $.pjax.reload({container:'#index'});
        }            
        else
        {
            $(\$form).trigger("reset");
            $("#message").html(result.message);
        }
    }).fail(function()
    {
        console.log("server-error");
    })
    return false;
})
JS;
$this->registerJs($script);
?>