<?php

use yii\grid\GridView;
use yii\data\ActiveDataProvider;
use frontend\models\Employee;
use yii\widgets\Pjax;
use yii\helpers\Html;

$dataProvider = new ActiveDataProvider([
    'query' => Employee::find(),
    'pagination' => [
        'pageSize' => 2,
    ],

]);
?>
<h2>Employee Data</h2>
<?php
Pjax::begin([
    'id' => 'pjax-list',
    'enablePushState' => false,
    'enableReplaceState' => false,
]);
echo GridView::widget([
    'dataProvider' => $dataProvider,
    // 'options' => [
    //     'data-pjax' => 0
    // ],
    'columns' => [
        'id',
        'first_name',
        [
            'attribute' => 'email',
            'label' => 'Email Address'
        ],
        [
            'class' => 'yii\grid\ActionColumn',
            'header' => 'Action',
            'headerOptions' => ['width' => '80'],
            'buttons' => [
                'update' => function ($url, $model) {
                    return Html::a('<span class="glyphicon glyphicon-pencil"></span>', $url, [
                        'title' => Yii::t('app', 'lead-update'),
                        'data-pjax' => '0',

                    ]);
                },

                'delete' => function ($url, $model) {

                    // return ((Yii::$app->user->can("employee/delete")) ? Html::a('<span class="glyphicon glyphicon-trash"></span>', $url, ['title' => Yii::t('app', 'Delete'), 'data' => ['confirm' => 'Are you sure you want to delete this item?', 'method' => 'post'], 'data-ajax' => '1']) : '');

                    return ((Yii::$app->user->can("/student/stu/delete")) ? Html::a('<span  class="glyphicon glyphicon-trash"></span>', false, ['class' => 'ajaxDelete', 'delete-url' => $url, 'pjax-container' => 'pjax-list', 'title'  => Yii::t('app', 'Delete'), 'data' => ['confirm' => 'Are you  sure you want to delete this item?']]) : '');
                }
            ],
        ]

    ]
]);
Pjax::end(); ?>
<?php $script = <<< JS
$(function () {

$(document).on('click', '.ajaxDelete', function () {
    alert('we are in delete');
    var deleteUrl = $(this).attr('delete-url');

    var pjaxContainer = $(this).attr('pjax-list');

    $.ajax({

        url: deleteUrl,

        type: "post",

        dataType: 'json',

        error: function (xhr, status, error) {

            alert('There was an error with your request.' + xhr.responseText);

        }

    }).done(function (data) {

        $.pjax.reload({ container: '#' + $.trim(pjaxContainer) });

    });

});

});


JS;
$this->registerJs($script);
?>