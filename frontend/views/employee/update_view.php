<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<h2>Update User</h2>
<img src='assets/images/<?= $model->photo; ?>' height="200" width="200">
<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data'], 'id' => 'update-form']); ?>

<?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>

<?= $form->field($model, 'photo')->fileInput()->label('Profile Photo'); ?>

<?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']); ?>

<?php ActiveForm::end(); ?>
<?php $script = <<< JS

$('#update-form').on('beforeSubmit',function(e)
{
e.preventDefault();
var formData = new FormData($('form')[0]);
$.ajax({
url:$('#update-form').attr('action'),
type: 'POST',
data: formData,
contentType: false,
cache: false,
processData: false,
dataType: 'json',
success: function (data) {
console.log(data);
$('#modal').modal('hide');
location.reload();
}
})
JS;
$this->registerJs($script);
?>