<?php

use yii\helpers\Html;
?>
<p>You have entered he following information</p>
<ul>
    <li><label>First Name</label>: <?= Html::encode($model->fname) ?></li>
    <li><label>Second Name</label>: <?= Html::encode($model->lname) ?></li>
    <li><label>Phone Number</label>: <?= Html::encode($model->phone) ?></li>
    <li><label>Email</label>: <?= Html::encode($model->email) ?></li>
    <li><label>Enter your favorite color</label>: <?= Html::encode($model->color) ?></li>
    <li><label>Date of Birth</label>: <?= Html::encode($model->dob) ?></li>
    <li><label>Gender</label>: <?= Html::encode($model->gender) ?></li>
    <li><label>Qualification</label>: <?= Html::encode($model->qual) ?></li>
    <li><label>Password</label>: <?= Html::encode($model->pass) ?></li>
    <li><label>Confirm Password</label>: <?= Html::encode($model->cpass) ?></li>
    <li><label>Upload Resume</label>: <?= Html::encode($model->resume) ?></li>
</ul>