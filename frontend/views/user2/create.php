<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model frontend\models\Users2 */

?>
<div class="users2-create">
    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>
</div>
